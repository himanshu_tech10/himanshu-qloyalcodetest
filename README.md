# Code Challenge

## Instruction for you mission:

fork this project, and provide your own github or github project.
Setup this project with all the libraries you needed to set a testing framework
Make sure you work on feature branch , not on master.
Naming convention for the feature branch should be "<YourName-QloyalCodeTest>".
Once you are complete push code to the feature branch only.
Use best practices to write code which you follow in your day to day development.

## Your mission

### REST API 

Go to URL : https://qa-challenges-lightbulb.atlassian.io
Create an automated regression for the below ACs
Using SOAPUI (using https://qa-challenges-lightbulb.atlassian.io/api/allmethods)

- AC1. I want to turn on and off my light
- AC2. I want my light only use a max of 60% of power

Once you are complete push code to the feature, and ensure you provide a README file explaining how to run your code


### WEB Automation
Go to URL : http://phptravels.com/demo/
Create an automated smoke test suite using http://devexpress.github.io/testcafe/ for the below ACs 
(you can do some manual set up, but this must be documented)
Ensure you provide a README file explaining how to run your code

- AC1. I want to be able book hotels, flights or tours
- AC2. I want to be able to create a cupon or promo-code and use it for tours only


## EXTRA POINTS

+ 10 points each


1. Go to https://github.com/awslabs/aws-device-farm-sample-app-for-ios and run the below feature files
- https://github.com/awslabs/aws-device-farm-sample-app-for-ios/blob/master/features/login_page.feature
- https://github.com/awslabs/aws-device-farm-sample-app-for-ios/blob/master/features/home_page.feature

2. Run Services automation regression in a CI tool like circle-ci, shippable, AWS codebuild

3. Deploy a Selenium grid with Docker and run phptravels smoke test in Chrome and Firefox

4. Provide performance test for https://qa-challenges-lightbulb.atlassian.io services

5. Run lighthouse performance test for http://phptravels.com/demo/ homepage and 3 other pages you think are relevant

6. Fire off SoapUI tests by sending the SoapUI project file to a docker container. 

## NOTE

If you are not able to complete the challenge with the tools requested,
but you have a framework you are familiar with, please submit the alternative with the relevant documentation 
on how to install and run your tool